<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\IntUsuarios;

/* @var $this yii\web\View */
/* @var $model app\models\IntDestacados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="int-destacados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 

        if($this->context->action->id === 'create'){
            echo $form->field($model, 'imagen')->hiddenInput(['class'=>'nueva_imagen','id'=>'nueva_imagen', 'value' => ''])->label(false); 
        }
        else{
            echo $form->field($model, 'imagen')->hiddenInput(['class'=>'nueva_imagen','id'=>'nueva_imagen', 'value' => $model->imagen])->label(false); 
        }
    ?>


    <?= 
    $form->field($model, 'usuario_id')->dropdownList(ArrayHelper::map(IntUsuarios::find()->all(), 'id', 'email'),

        [
            'prompt' => 'Seleccione Usuario'
        ]
        
    ) 

    ?>


    <?= $form->field($model, 'nombre')->textInput() ?>



    <!-- _________________ Imagen Actual _________________ -->   
    <?php 

        if($this->context->action->id === 'update'){


                if($model->imagen != ''){
    ?>
                    <div class="form-group">

                        <label class="control-label">Imagen Actual</label> <br>

                        <img src='<?php echo $model->imagen ?>' width='200'/>

                        <div class="help-block"></div>
                    </div>
    <?php
                }
                else{
                    
                    $sin_imagen = Yii::getAlias('@web').'/img/sin_imagen.jpg';
    ?>
                    <div class="form-group">

                        <label class="control-label">Imagen Actual</label> <br>

                        <img src='<?php echo $sin_imagen ?>' width='200'/>

                        <div class="help-block"></div>
                    </div>
    <?php
                }
                    
        }

    ?>
    <!-- _________________ Imagen Actual _________________ -->  

    

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
