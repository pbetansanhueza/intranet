<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IntDestacadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Destacados';
$this->params['breadcrumbs'][] = $this->title;
?>
<style></style>
<div class="int-destacados-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Destacado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'usuario_id',
            'nombre',
            'descripcion:ntext',

             [ 'attribute' => 'imagen', 'label' => 'Imagen' , 'format' => 'image' , 'value' => function($data){

                    if($data->imagen != ''){
                        return $data->imagen;
                    }
                    else{
                        return Yii::getAlias('@web').'/img/sin_imagen.jpg';
                    }
                
                }, 
                'contentOptions' => ['class' => 'img_grilla']
             ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
