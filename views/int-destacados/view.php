<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\IntUsuarios;

/* @var $this yii\web\View */
/* @var $model app\models\IntDestacados */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Destacados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-destacados-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php /*echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'usuario_id',
            'nombre',
            'descripcion:ntext',
            'imagen:ntext',
            'created_at',
            'updated_at',
        ],
    ]) */ ?>



    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Id</th>
                <td><?= $model->id ?></td>
            </tr>
            <tr>
                <th>Creado por</th>
                <td>
                    <?php 

                    $consulta = IntUsuarios::find()
                    ->where(['id' => $model->usuario_id])->asArray()->one();
                    
                    echo $consulta['email'];

                    ?>
                    
                </td>
            </tr>
            <tr>
                <th>Nombre destacado</th>
                <td><?= $model->nombre ?></td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td><?= $model->descripcion ?></td>
            </tr>
            <tr>
                <th>Fecha creación</th>
                <td><?= $model->created_at ?></td>
            </tr>
            <tr>
                <th>Imagen</th>
                <td>
                    <?php 
                    if($model->imagen != ''){
                    ?>
                        <img src="<?php echo $model->imagen ?>" width="200"/>
                    <?php 
                    }
                    else{
                    ?>
                        <img src="<?php echo Yii::getAlias('@web') ?>/img/sin_imagen.jpg" width="200"/>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        </tbody>
    </table>

</div>
