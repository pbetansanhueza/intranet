<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IntAreas */

$this->title = 'Update Int Areas: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Int Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_area, 'url' => ['view', 'id' => $model->id_area]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="int-areas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
