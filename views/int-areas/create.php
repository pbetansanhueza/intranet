<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IntAreas */

$this->title = 'Create Int Areas';
$this->params['breadcrumbs'][] = ['label' => 'Int Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-areas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
