<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IntControlUsuarios */

$this->title = 'Create Int Control Usuarios';
$this->params['breadcrumbs'][] = ['label' => 'Int Control Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-control-usuarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
