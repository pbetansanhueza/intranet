<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IntControlUsuarios */

$this->title = 'Update Int Control Usuarios: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Int Control Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="int-control-usuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
