<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\IntAreas;
use app\models\IntControlUsuarios;

/* @var $this yii\web\View */
/* @var $model app\models\IntUsuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="int-usuarios-form">


<?php $form = ActiveForm::begin(); ?>

    <?php 

        if($this->context->action->id === 'create'){
            echo $form->field($model, 'url_imagen')->hiddenInput(['class'=>'nueva_imagen','id'=>'nueva_imagen', 'value' => ''])->label(false); 
        }
        else{
            echo $form->field($model, 'url_imagen')->hiddenInput(['class'=>'nueva_imagen','id'=>'nueva_imagen', 'value' => $model->url_imagen])->label(false); 
        }
    ?>


    <?= 

    $form->field($model, 'area_id')->dropdownList(ArrayHelper::map(IntAreas::find()->all(), 'id_area', 'nombre'),

        [
            'prompt' => 'Seleccione Área'
        ]


        
    ) 

    ?>


    <?= 

    $form->field($model, 'id_controlusuario')->dropdownList(ArrayHelper::map(IntControlUsuarios::find()->all(), 'id', 'nombre'),

        [
            'prompt' => 'Seleccione Control Usuario'
        ]


        
    )

    ?>
    
    <?= $form->field($model, 'email')->textInput() ?>

    <?php 

        if($this->context->action->id === 'create'){
            echo $form->field($model, 'password')->passwordInput(['value' => '']);
        }
        else{
            echo $form->field($model, 'password')->passwordInput(['value' => $model->literal_pass]);
        }

    ?>


    <?php 

        if($this->context->action->id === 'update'){


                if($model->url_imagen != ''){
    ?>
                    <div class="form-group">

                        <label class="control-label">Imagen Actual</label> <br>

                        <img src='<?php echo $model->url_imagen ?>' width='200'/>

                        <div class="help-block"></div>
                    </div>
    <?php
                }
                else{
                    
                    $sin_imagen = Yii::getAlias('@web').'/img/sin_imagen.jpg';
    ?>
                    <div class="form-group">

                        <label class="control-label">Imagen Actual</label> <br>

                        <img src='<?php echo $sin_imagen ?>' width='200'/>

                        <div class="help-block"></div>
                    </div>
    <?php
                }
                    
        }

    ?>



    


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
