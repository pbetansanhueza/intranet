<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IntUsuarios */

$this->title = 'Modificar Usuario: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';


?>

<style>

#preview .buttons .ok{

  background-image: url('<?php echo Yii::getAlias('@web') ?>/img/Ok.png');
  background-repeat: no-repeat;
}
#preview .buttons .ok:hover{
  background-image: url('<?php echo Yii::getAlias('@web') ?>/img/OkGreen.png');
}

#preview .buttons .cancel{
  background-image: url('<?php echo Yii::getAlias('@web') ?>/img/Cancel.png');
  background-repeat: no-repeat;
}

#preview .buttons .cancel:hover{
  background-image: url('<?php echo Yii::getAlias('@web') ?>/img/CancelRed.png');
}
.jcrop-vline,
.jcrop-hline {
	background-image: url('<?php echo Yii::getAlias('@web') ?>/img/Jcrop.gif');
}
.cropme{
  background-image: url('<?php echo Yii::getAlias('@web') ?>/img/UploadLight.png');

}

.cropme:hover{
  background-image: url('<?php echo Yii::getAlias('@web') ?>/img/UploadDark.png');
}

</style>

<div class="int-usuarios-update">


	<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#formulario">Formulario</a></li>
		  <li><a data-toggle="tab" href="#imagen">Imagen</a></li>
	</ul>

	<div class="tab-content">


			<!-- Formulario -->
		    <div id="formulario" class="tab-pane fade in active">


				    <h1><?= Html::encode($this->title) ?></h1>

				    <?= $this->render('_form', [
				        'model' => $model,
				    ]) ?>

			</div>
			<!-- Formulario -->

			<!-- Adjuntar Imagen -->
		  	<div id="imagen" class="tab-pane fade">

			    <h3>Imagen</h3>
			    <p>
			    		<div class="container">
							<div class="row">
								<div class="span12">
									<div class="panel panel-heading">
										<div class="span4"></div>
										<div class="span4"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="panel panel-body">
									<div class="span4 cropme" id="landscape" style="width: 500px; height: 250px;"></div>
									<div class="span4 cropme" id="portrait" style="width: 500px; height: 500px;"></div>
								</div>
							</div>
						</div>
			    </p>
			</div>
			<!-- Adjuntar Imagen -->

	</div>

</div>
