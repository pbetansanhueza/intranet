<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model app\models\IntUsuarios */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="int-usuarios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php /*echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            'url_imagen:url',
            'intAreas.nombre',
            //'id_controlusuario',
             'intControlUsuarios.nombre',
            'created_at',
            'updated_at'
        ],
    ]) */ ?>




    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Id</th>
                <td><?= $model->id ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?= $model->email ?></td>
            </tr>
            <tr>
                <th>Área</th>
                <td><?= $model->intAreas->nombre ?></td>
            </tr>
            <tr>
                <th>Control usuario</th>
                <td><?= $model->intControlUsuarios->nombre ?></td>
            </tr>
            <tr>
                <th>Fecha creación</th>
                <td><?= $model->created_at ?></td>
            </tr>
            <tr>
                <th>Imagen</th>
                <td>
                    <?php 
                    if($model->url_imagen != ''){
                    ?>
                        <img src="<?php echo $model->url_imagen ?>" width="200"/>
                    <?php 
                    }
                    else{
                    ?>
                        <img src="<?php echo Yii::getAlias('@web') ?>/img/sin_imagen.jpg" width="200"/>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        </tbody>
    </table> 

</div>
