<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IntUsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="int-usuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [ 'attribute' => 'EMPRESA' , 'value' => 'intTrabajadores.EMPRESA'], // Campo de tabla externa
            [ 'attribute' => 'FICHA' , 'value' => 'intTrabajadores.FICHA' , 'label' => 'Rut'], // Campo de tabla externa
            [ 'attribute' => 'NOMBRE' , 'value' => 'intTrabajadores.NOMBRE' , 'label' => 'Nombres'], // Campo de tabla externa
            [ 'attribute' => 'APELLIDO_PATERNO' , 'value' => 'intTrabajadores.APELLIDO_PATERNO' , 'label' => 'Apellido'], // Campo de tabla externa
            [ 'attribute' => 'email' , 'value' => 'email'], 
            [ 'attribute' => 'CARGO' , 'value' => 'intTrabajadores.CARGO' , 'label' => 'Cargo'], // Campo de tabla externa


            [ 'attribute' => 'area_id' , 'value' => 'intAreas.nombre'], // Campo de tabla externa

            [ 'attribute' => 'url_imagen' , 'label' => 'Imagen' , 'format' => 'image' ,'value' => function($data){

                    if($data->url_imagen != ''){
                        return $data->url_imagen;
                    }
                    else{
                        return Yii::getAlias('@web').'/img/sin_imagen.jpg';
                    }
                
                }, 
                'contentOptions' => ['class' => 'img_grilla']
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
