<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IntNoticiasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-noticias-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Noticia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_noticia',
            'titulo',
            'sub_titulo',
            'orden',
            //'texto:ntext',
            //'imagen:ntext',
            //'activa',
            //'usuario_id',
            //'created_at',
            //'updated_at',
            //'orden',
            [ 'attribute' => 'imagen', 'label' => 'Imagen' , 'format' => 'image' , 'value' => function($data){

                    if($data->imagen != ''){
                        return $data->imagen;
                    }
                    else{
                        return Yii::getAlias('@web').'/img/sin_imagen.jpg';
                    }
                
                }, 
                'contentOptions' => ['class' => 'img_grilla']
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
