<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\IntUsuarios;

/* @var $this yii\web\View */
/* @var $model app\models\IntNoticias */

$this->title = $model->id_noticia;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-noticias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_noticia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_noticia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php 
    /*echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_noticia',
            'titulo',
            'sub_titulo',
            'texto:ntext',
            'imagen:ntext',
            'activa',
            'usuario_id',
            'created_at',
            'updated_at',
            'orden',
        ],
    ]) */ ?>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Id</th>
                <td><?= $model->id_noticia ?></td>
            </tr>
            <tr>
                <th>Publicada</th>
                <td>
                    <?php 
                    if($model->activa == 0){
                        echo "No";
                    }
                    else{
                        echo "Si";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <th>Posición</th>
                <td><?= $model->orden ?></td>
            </tr>
            <tr>
                <th>Creada por</th>
                <td>
                    <?php 

                    $consulta = IntUsuarios::find()
                    ->where(['id' => $model->usuario_id])->asArray()->one();
                    
                    echo $consulta['email'];

                    ?>
                    
                </td>
            </tr>
            <tr>
                <th>Titulo</th>
                <td><?= $model->titulo ?></td>
            </tr>
            <tr>
                <th>Sub titulo</th>
                <td><?= $model->sub_titulo ?></td>
            </tr>
            <tr>
                <th>Fecha creación</th>
                <td><?= $model->created_at ?></td>
            </tr>
            <tr>
                <th>Imagen</th>
                <td>
                    <?php 
                    if($model->imagen != ''){
                    ?>
                        <img src="<?php echo $model->imagen ?>" width="200"/>
                    <?php 
                    }
                    else{
                    ?>
                        <img src="<?php echo Yii::getAlias('@web') ?>/img/sin_imagen.jpg" width="200"/>
                    <?php
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <th>Texto</th>
                <td><?= $model->texto ?></td>
            </tr>
        </tbody>
    </table>


</div>
