<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

    <!-- CSS --> 
    <link href="<?php echo Yii::getAlias('@web') ?>/css/font-awesome.min.css" rel="stylesheet">


    <!-- Crop -->
    <link href="<?php echo Yii::getAlias('@web') ?>/css/crop/jquery.Jcrop.css" rel="stylesheet">
    <link href="<?php echo Yii::getAlias('@web') ?>/css/crop/style-example.css" rel="stylesheet">
    <!-- Crop -->


</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    $logo_desa = Yii::getAlias('@web')."/img/logo-desa.png";
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Admin', 'url' => ['/site/admin']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">


        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; DESA <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>



</body>

    <!-- Crop -->
    <script src="<?php echo Yii::getAlias('@web') ?>/js/crop/jquery.Jcrop.js"></script>
    <script src="<?php echo Yii::getAlias('@web') ?>/js/crop/jquery.SimpleCropper.js"></script> 

    <!-- JS -->
    <script src="<?php echo Yii::getAlias('@web') ?>/js/funciones.js"></script>



</html>
<?php $this->endPage() ?>
