<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\IntTrabajadores */

$this->title = $model[0]['NOMBRE'];
$this->params['breadcrumbs'][] = ['label' => 'Int Trabajadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-usuarios-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr> <th>Empresa</th> <td> <?= $model[0]['EMPRESA'] ?> </td> </tr>
            <tr><th>Área</th> <td>  <?= $model[0]['AREA'] ?> </td></tr>
            <tr><th>Cargo</th> <td>  <?= $model[0]['CARGO'] ?> </td></tr>
            <tr><th>Fecha Inicio</th> <td>  <?= $model[0]['FECHA_INICIO'] ?> </td></tr>
            <tr><th>Correo</th> <td> <?= $model[0]['CORREOELEC'] ?> </td></tr>
            <tr><th>Móvil</th> <td>   </td></tr>
            <tr><th>Directo</th> <td>  </td></tr>
        </tbody>
    </table>

</div>
