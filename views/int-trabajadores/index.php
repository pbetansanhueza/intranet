<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IntUsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Int Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="int-usuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'CORREOELEC',
            'NOMBRE',
            //'url_imagen:url',
            //'password',
            //'remember_token',
            'EMPRESA',
            'CARGO',
            //'created_at',
            //'updated_at',
            //'auth_key',
            //'access_token',
            //'username',
            //'name',

            [ 'class' => 'yii\grid\ActionColumn' , 
              'template' => '{view}', 
               'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=int-trabajadores/view&correoelec='.$model->CORREOELEC;
                        return $url;
                    }
                }
                ]
            ],

    ]); ?>

</div>
