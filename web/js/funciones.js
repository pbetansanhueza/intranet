/* CROPPER */
		

$(document).ready(function() {


	function readURL(input) {

        if (input.files && input.files[0]) {

            var reader    = new FileReader();
            
            reader.onload = function (e) {

                $('#blah').attr('src', e.target.result);
                $('.cont_upload_img').show(200);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this); //
    });

    

    // ___________ Init Simple Cropper ___________
    $('.cropme').simpleCropper();

    $('#portrait').hide();

    $('.switch').click(function (){

            $(this).text("Switch to "+($('#portrait').is(":visible") ? "Portrait" : "Landscape"));
            $('#portrait').toggle();
            $('#landscape').toggle();
    });


    var myVar;

    $('.ok').click(function() {

        myVar = setInterval(function(){ nueva_imagen() }, 1000);

    });


    function nueva_imagen() {
        var img = $('.cropme').children('img');
        $('.nueva_imagen').val(img.attr('src'));
        myStopFunction();
    }

    function myStopFunction() {
        clearInterval(myVar);
    }

});
