<?php

namespace app\controllers;

use Yii;
use app\models\IntTrabajadores;
use app\models\IntTrabajadoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\db\Query; 

/**
 * IntTrabajadoresController implements the CRUD actions for IntTrabajadores model.
 */
class IntTrabajadoresController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view','update','index'], //En que vistas aplicare control de acceso (login)
                'rules' => [
                    [
                        'actions' => ['logout','view','update','index'],  //Que vistas puede ver estando logueado
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IntUsuarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new IntTrabajadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

   
    public function actionView($correoelec)
    {
        return $this->render('view', [
            'model' => $this->findModel($correoelec),
        ]);
    }

     
    protected function findModel($correoelec)
    {

        $query          = new Query;

        $provider       = new ArrayDataProvider([
                'allModels' => $query->from('Int_Trabajadores')->where(['CORREOELEC' => $correoelec])->all(),
                'sort'      => [
                    'attributes' => ['CORREOELEC','FICHA','NOMBRE','APELLIDO_PATERNO','APELLIDO_MATERNO','EMPRESA','CARGO','FECHA_INICIO'],
                ],
                'pagination'   => [
                    'pageSize' => 10,
                ],
        ]);

        $model = $provider->getModels();
        return $model;

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
}
