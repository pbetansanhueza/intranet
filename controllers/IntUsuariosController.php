<?php

namespace app\controllers;

use Yii;
use app\models\IntUsuarios;
use app\models\IntUsuariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * IntUsuariosController implements the CRUD actions for IntUsuarios model.
 */
class IntUsuariosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view','update','index'], //En que vistas aplicare control de acceso (login)
                'rules' => [
                    [
                        'actions' => ['logout','view','update','index'],  //Que vistas puede ver estando logueado
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IntUsuarios models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel  = new IntUsuariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single IntUsuarios model.
    * @param integer $id
    * @return mixed
    * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IntUsuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model             = new IntUsuarios();

        if ($model->load(Yii::$app->request->post())) {


            $model->created_at       = date('Y-m-d');
            $model->updated_at       = date('Y-m-d');

            $key                     = Yii::$app->getSecurity()->generateRandomString();
            $encryptedPassw          = utf8_encode(Yii::$app->getSecurity()->encryptByPassword($model->password, $key));
            $model->literal_pass     = $model->password;
            $model->password         = $encryptedPassw;
            $model->auth_key         = $key; 

            if(isset($model->url_imagen)){
                $data_imagen       = $model->url_imagen;
                $model->url_imagen = $data_imagen;
            }


            if($model->save()){

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IntUsuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model    = $this->findModel($id);

        //$encryptedPassw = Yii::$app->getSecurity()->encryptByPassword($model->literal_pass, $model->auth_key);
        //$decrypt        = Yii::$app->getSecurity()->decryptByPassword($encryptedPassw, $model->auth_key);

        if ($model->load(Yii::$app->request->post())) {


            $model->updated_at       = date('Y-m-d');
            $encryptedPassw          = utf8_encode(Yii::$app->getSecurity()->encryptByPassword($model->password, $model->auth_key));
            $model->literal_pass     = $model->password;
            $model->password         = $encryptedPassw;

            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IntUsuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IntUsuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IntUsuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IntUsuarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
