<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[IntAreas]].
 *
 * @see IntAreas
 */
class IntAreasQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IntAreas[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IntAreas|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
