<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[IntTrabajadores]].
 *
 * @see IntTrabajadores
 */
class IntTrabajadoresQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IntTrabajadores[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IntTrabajadores|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
