<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[IntNoticias]].
 *
 * @see IntNoticias
 */
class IntNoticiasQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IntNoticias[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IntNoticias|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
