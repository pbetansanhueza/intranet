<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INT_NOTICIAS".
 *
 * @property int $id_noticia
 * @property string $titulo
 * @property string $sub_titulo
 * @property string $texto
 * @property string $imagen
 * @property int $activa
 * @property int $usuario_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $orden
 */
class IntNoticias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INT_NOTICIAS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'titulo', 'sub_titulo' , 'texto', 'orden'], 'required', 'message' => '{attribute} es requerido.'],
            [['titulo', 'sub_titulo', 'texto', 'imagen'], 'string'],
            [[ 'created_at', 'updated_at'], 'safe'],
            [['activa', 'usuario_id', 'orden'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_noticia' => 'Id Noticia',
            'titulo'     => 'Titulo',
            'sub_titulo' => 'Sub Titulo',
            'texto'      => 'Texto',
            'imagen'     => 'Imagen',
            'activa'     => 'Activa',
            'usuario_id' => 'Usuario ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'orden'      => 'Orden',
        ];
    }

    /**
     * @inheritdoc
     * @return IntNoticiasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IntNoticiasQuery(get_called_class());
    }

    public function getIntUsuarios()  //'campo tabla externa' => 'campo presente tabla'
    {
        return $this->hasOne(IntUsuarios::className(), ['id' => 'usuario_id']);  
    }
}
