<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[IntUsuarios]].
 *
 * @see IntUsuarios
 */
class IntUsuariosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IntUsuarios[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IntUsuarios|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
