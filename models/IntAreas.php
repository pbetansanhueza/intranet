<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INT_AREAS".
 *
 * @property int $id_area
 * @property string $nombre
 * @property string $control_usuario
 * @property string $created_at
 * @property string $updated_at
 */
class IntAreas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INT_AREAS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'control_usuario', 'created_at', 'updated_at'], 'required'],
            [['nombre', 'control_usuario'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_area' => 'Id Area',
            'nombre' => 'Nombre Área',
            'control_usuario' => 'Control Usuario',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return IntAreasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IntAreasQuery(get_called_class());
    }
}
