<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IntTrabajadores;

/**
 * IntUsuariosSearch represents the model behind the search form of `app\models\IntUsuarios`.
 */
class IntTrabajadoresSearch extends IntTrabajadores
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CORREOELEC','FICHA'], 'string'],
            [['CORREOELEC', 'FICHA', 'NOMBRE', 'APELLIDO_PATERNO', 'APELLIDO_MATERNO', 'Dimension1', 'Dimension2', 'DIRECCION', 'COMUNA', 'CIUDAD', 'AREA', 'NACION','FECHA_NACIMIENTO','SEXO','ESTADO_CIVIL','CARGO','FECHA_INICIO','EMPRESA'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntTrabajadores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CORREOELEC' => $this->CORREOELEC,
            'FICHA'      => $this->FICHA,
            'NOMBRE'     => $this->NOMBRE,
            'EMPRESA'    => $this->EMPRESA,
        ]);


        $query->andFilterWhere(['like', 'CORREOELEC', $this->CORREOELEC])
            ->andFilterWhere(['like', 'FICHA', $this->FICHA])
            ->andFilterWhere(['like', 'NOMBRE', $this->NOMBRE])
            ->andFilterWhere(['like', 'APELLIDO_PATERNO', $this->APELLIDO_PATERNO])
            ->andFilterWhere(['like', 'APELLIDO_MATERNO', $this->APELLIDO_MATERNO])
            ->andFilterWhere(['like', 'Dimension1', $this->Dimension1])
            ->andFilterWhere(['like', 'DIRECCION', $this->DIRECCION])
            ->andFilterWhere(['like', 'COMUNA', $this->COMUNA])
            ->andFilterWhere(['like', 'CIUDAD', $this->CIUDAD])
            ->andFilterWhere(['like', 'AREA', $this->AREA])
            ->andFilterWhere(['like', 'NACION', $this->NACION])
            ->andFilterWhere(['like', 'FECHA_NACIMIENTO', $this->FECHA_NACIMIENTO])
            ->andFilterWhere(['like', 'SEXO', $this->SEXO])
            ->andFilterWhere(['like', 'ESTADO_CIVIL', $this->ESTADO_CIVIL])
            ->andFilterWhere(['like', 'CARGO', $this->CARGO])
            ->andFilterWhere(['like', 'FECHA_INICIO', $this->FECHA_INICIO])
            ->andFilterWhere(['like', 'EMPRESA', $this->EMPRESA]);

        return $dataProvider;
    }
}
