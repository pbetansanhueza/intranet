<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INT_CONTROLUSUARIOS".
 *
 * @property int $id
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 */
class IntControlUsuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INT_CONTROLUSUARIOS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return IntControlUsuariosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IntControlUsuariosQuery(get_called_class());
    }
}
