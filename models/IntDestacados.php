<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INT_DESTACADOS".
 *
 * @property int $id
 * @property int $usuario_id
 * @property string $nombre
 * @property string $descripcion
 * @property string $imagen
 * @property string $created_at
 * @property string $updated_at
 */
class IntDestacados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INT_DESTACADOS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'nombre' , 'descripcion'], 'required', 'message' => '{attribute} es requerido.'],
            [['usuario_id'], 'integer'],
            [['nombre', 'descripcion', 'imagen'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'imagen' => 'Imagen',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return IntDestacadosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IntDestacadosQuery(get_called_class());
    }

    public function getIntUsuarios()  //'campo tabla externa' => 'campo presente tabla'
    {
        return $this->hasOne(IntUsuarios::className(), ['id' => 'usuario_id']);  
    }
}
