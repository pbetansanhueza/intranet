<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INT_TRABAJADORES".
 *
 * @property string $CORREOELEC
 * @property string $FICHA
 * @property string $NOMBRE
 * @property string $APELLIDO_PATERNO
 * @property string $APELLIDO_MATERNO
 * @property string $Dimension1
 * @property string $Dimension2
 * @property string $DIRECCION
 * @property string $COMUNA
 * @property string $CIUDAD
 * @property string $AREA
 * @property string $NACION
 * @property string $FECHA_NACIMIENTO
 * @property string $SEXO
 * @property string $ESTADO_CIVIL
 * @property string $CARGO
 * @property string $FECHA_INICIO
 * @property string $EMPRESA
 */
class IntTrabajadores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INT_TRABAJADORES';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CORREOELEC', 'FICHA', 'NOMBRE', 'APELLIDO_PATERNO', 'APELLIDO_MATERNO', 'Dimension1', 'Dimension2', 'DIRECCION', 'COMUNA', 'CIUDAD', 'AREA', 'NACION', 'SEXO', 'ESTADO_CIVIL', 'CARGO', 'EMPRESA'], 'string'],
            [['FICHA', 'EMPRESA'], 'required'],
            [['FECHA_NACIMIENTO', 'FECHA_INICIO'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CORREOELEC' => 'Correoelec',
            'FICHA' => 'Ficha',
            'NOMBRE' => 'Nombre',
            'APELLIDO_PATERNO' => 'Apellido  Paterno',
            'APELLIDO_MATERNO' => 'Apellido  Materno',
            'Dimension1' => 'Dimension1',
            'Dimension2' => 'Dimension2',
            'DIRECCION' => 'Direccion',
            'COMUNA' => 'Comuna',
            'CIUDAD' => 'Ciudad',
            'AREA' => 'Area',
            'NACION' => 'Nacion',
            'FECHA_NACIMIENTO' => 'Fecha  Nacimiento',
            'SEXO' => 'Sexo',
            'ESTADO_CIVIL' => 'Estado  Civil',
            'CARGO' => 'Cargo',
            'FECHA_INICIO' => 'Fecha  Inicio',
            'EMPRESA' => 'Empresa',
        ];
    }

    /**
     * @inheritdoc
     * @return IntTrabajadoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IntTrabajadoresQuery(get_called_class());
    }

    public function getIntTrabajadores()
    {
       return $this->hasOne(IntUsuarios::class, 'email', 'CORREOELEC' );
    }
}
