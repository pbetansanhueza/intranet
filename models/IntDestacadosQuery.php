<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[IntDestacados]].
 *
 * @see IntDestacados
 */
class IntDestacadosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IntDestacados[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IntDestacados|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
