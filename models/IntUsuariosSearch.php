<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IntUsuarios;


/**
 * IntUsuariosSearch represents the model behind the search form of `app\models\IntUsuarios`.
 */
class IntUsuariosSearch extends IntUsuarios
{
    /**
     * @inheritdoc
     */

    public $FICHA;   //Aca se declaran los campos de una tabla externa los cuales se necesiten llamar...El nombre debe ser igual al de la columna
    public $EMPRESA;
    public $NOMBRE;
    public $APELLIDO_PATERNO;
    public $CARGO;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['area_id', 'id_controlusuario','email','FICHA','EMPRESA','NOMBRE','APELLIDO_PATERNO','CARGO'], 'string'],
            [['email', 'url_imagen', 'password', 'remember_token', 'control_usuario', 'created_at', 'updated_at', 'auth_key', 'access_token', 'username', 'name', 'area_id', 'id_controlusuario','FICHA','EMPRESA','NOMBRE','APELLIDO_PATERNO','CARGO'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntUsuarios::find();
        $query->joinWith(['intAreas']);
        $query->joinWith(['intControlUsuarios']);
        $query->joinWith(['intTrabajadores']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //-------- De menor a mayor y viceversa ---------
        $dataProvider->sort->attributes['email'] = [
            'asc'  => [ 'email' => SORT_ASC  ],
            'desc' => [ 'email' => SORT_DESC ]
        ];

        $dataProvider->sort->attributes['nombre'] = [
            'asc'  => [ 'intAreas.nombre' => SORT_ASC  ],
            'desc' => [ 'intAreas.nombre' => SORT_DESC ]
        ];

        $dataProvider->sort->attributes['nombre'] = [
            'asc'  => [ 'intControlUsuarios.nombre' => SORT_ASC  ],
            'desc' => [ 'intControlUsuarios.nombre' => SORT_DESC ]
        ];

        $dataProvider->sort->attributes['FICHA'] = [  //Debe ser el mismo nombre (titulo) que esta en la columna de la vista
            'asc'  => [ 'INT_TRABAJADORES.FICHA' => SORT_ASC  ],
            'desc' => [ 'INT_TRABAJADORES.FICHA' => SORT_DESC ]
        ];

        $dataProvider->sort->attributes['EMPRESA'] = [  //Debe ser el mismo nombre (titulo) que esta en la columna de la vista
            'asc'  => [ 'INT_TRABAJADORES.EMPRESA' => SORT_ASC  ],
            'desc' => [ 'INT_TRABAJADORES.EMPRESA' => SORT_DESC ]
        ];
        $dataProvider->sort->attributes['NOMBRE'] = [  //Debe ser el mismo nombre (titulo) que esta en la columna de la vista
            'asc'  => [ 'INT_TRABAJADORES.NOMBRE' => SORT_ASC  ],
            'desc' => [ 'INT_TRABAJADORES.NOMBRE' => SORT_DESC ]
        ];
        $dataProvider->sort->attributes['APELLIDO_PATERNO'] = [  //Debe ser el mismo nombre (titulo) que esta en la columna de la vista
            'asc'  => [ 'INT_TRABAJADORES.APELLIDO_PATERNO' => SORT_ASC  ],
            'desc' => [ 'INT_TRABAJADORES.APELLIDO_PATERNO' => SORT_DESC ]
        ];
        $dataProvider->sort->attributes['CARGO'] = [  //Debe ser el mismo nombre (titulo) que esta en la columna de la vista
            'asc'  => [ 'INT_TRABAJADORES.CARGO' => SORT_ASC  ],
            'desc' => [ 'INT_TRABAJADORES.CARGO' => SORT_DESC ]
        ];
        //-------- De menor a mayor y viceversa ---------




        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'INT_USUARIOS.id' => $this->id,
            'created_at'      => $this->created_at,
            'updated_at'      => $this->updated_at
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            //->andFilterWhere(['like', 'url_imagen', $this->url_imagen])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'remember_token', $this->remember_token])
            ->andFilterWhere(['like', 'control_usuario', $this->control_usuario])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'INT_AREAS.nombre', $this->area_id])
            ->andFilterWhere(['like', 'INT_CONTROLUSUARIOS.nombre', $this->id_controlusuario])
            ->andFilterWhere(['like', 'INT_TRABAJADORES.FICHA', $this->FICHA ])
            ->andFilterWhere(['like', 'INT_TRABAJADORES.EMPRESA', $this->EMPRESA ])
            ->andFilterWhere(['like', 'INT_TRABAJADORES.NOMBRE', $this->NOMBRE ])
            ->andFilterWhere(['like', 'INT_TRABAJADORES.APELLIDO_PATERNO', $this->APELLIDO_PATERNO ])
            ->andFilterWhere(['like', 'INT_TRABAJADORES.CARGO', $this->CARGO ]); 


        return $dataProvider;
    }
}
