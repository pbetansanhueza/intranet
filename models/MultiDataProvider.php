<?php

namespace app\models;


class MultiDataProvider extends \yii\base\BaseObject
    {
        
        public function __construct() {
            

            
        }
        
        //create merged array from many CSqlDataProviders
        public function createOneArray($dataProviders)
        {
            $mergedArray = array();
            
            foreach($dataProviders as $dataProvider)
            {
                $data = $dataProvider->sql;
                for ($i=0;$i<count($dataProvider->sql);$i++)
                {
                    $myData = $data[$i];
                    array_push($mergedArray, $myData);
                }
            }
            
            return $mergedArray;
        }
        
    
        
    }