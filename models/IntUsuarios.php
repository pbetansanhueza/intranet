<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INT_USUARIOS".
 *
 * @property int $id
 * @property string $email
 * @property string $url_imagen
 * @property string $password
 * @property string $remember_token
 * @property int $area_id
 * @property string $control_usuario
 * @property string $created_at
 * @property string $updated_at
 * @property string $auth_key
 * @property string $access_token
 * @property string $username
 * @property string $name
 */
class IntUsuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
 

    public static function tableName()
    {
        return 'INT_USUARIOS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'area_id' , 'id_controlusuario' ,'created_at', 'updated_at','password'], 'required', 'message' => '{attribute} es requerido.'],
            [['id','area_id', 'id_controlusuario'], 'integer'],
            [['email', 'url_imagen' ,'password', 'literal_pass', 'remember_token', 'auth_key', 'access_token', 'username', 'name', 'email'], 'string'],
            [['created_at', 'updated_at', 'FICHA'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'             => 'Email',
            'url_imagen'        => 'Url Imagen',
            'password'          => 'Password',
            'area_id'           => 'Área',
            'id_controlusuario' => 'Control Usuario',
            'created_at'        => 'Fecha creación',
            'updated_at'        => 'Updated At',
            'FICHA'             => 'Ficha' 
        ];
    }



    /**
     * @inheritdoc
     * @return IntUsuariosQuery the active query used by this AR class.
     */

    public static function find()
    {
        return new IntUsuariosQuery(get_called_class());
    }
    public function getIntAreas()
    {

        return $this->hasOne(IntAreas::className(), ['id_area' => 'area_id']); //'campo tabla externa' => 'campo presente tabla'
    }
    public function getIntControlUsuarios()
    {
        return $this->hasOne(IntControlUsuarios::className(), ['id' => 'id_controlusuario']); 
    }
    public function getIntTrabajadores()
    {
        return $this->hasOne(IntTrabajadores::className(), ['CORREOELEC' => 'email']);
    }


}
