<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[IntControlUsuarios]].
 *
 * @see IntControlUsuarios
 */
class IntControlUsuariosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IntControlUsuarios[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IntControlUsuarios|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
