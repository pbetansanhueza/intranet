<?php

return [
    'class'    => 'yii\db\Connection',
    //'dsn'      => 'mysql:host=localhost;dbname=yii2basic',
    'dsn'      => 'sqlsrv:Server=10.0.0.2;Database=INTRANET_DESARROLLO', // MS SQL Server, sqlsrv driver
    'username' => 'sa',
    'password' => 'desakey',
    'charset'  => 'utf8', 

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
